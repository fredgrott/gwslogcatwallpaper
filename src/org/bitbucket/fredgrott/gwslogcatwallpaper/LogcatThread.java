package org.bitbucket.fredgrott.gwslogcatwallpaper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// TODO: Auto-generated Javadoc
/**
 * The Class LogcatThread.
 */
public abstract class LogcatThread extends Thread {
	
	/** The Constant LOGCAT_CMD. */
	private static final String[] LOGCAT_CMD = new String[] { "logcat" };
	
	/** The Constant BUFFER_SIZE. */
	private static final int BUFFER_SIZE = 1024;
	
	/** The m lines. */
	private int mLines = 0;
	
	/** The m logcat proc. */
	private Process mLogcatProc = null;
	
	/** The m running. */
	private boolean mRunning = false;
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		mRunning = true;
		
		try	{
			mLogcatProc = Runtime.getRuntime().exec(LOGCAT_CMD);
		} catch (IOException e) {
			onError("Can't start " + LOGCAT_CMD[0], e);
			return;
		}

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(mLogcatProc.getInputStream()), BUFFER_SIZE);

			String line;
			while (mRunning && (line = reader.readLine()) != null) {
				if (!mRunning) {
					break;
				}
				
				if (line.length() == 0) {
					continue;
				}
				
				onNewline(line);
				mLines++;
			}
		} catch (IOException e) {
			onError("Error reading from process " + LOGCAT_CMD[0], e);
		} finally {
			if (reader != null) {
				try { 
					reader.close(); 
				} catch (IOException e) {
				}
			}
			stopLogcat();
		}
	}

	/**
	 * Stop logcat.
	 */
	public void stopLogcat() {
		if (mLogcatProc == null)
			return;
	        
		mLogcatProc.destroy();
		mLogcatProc = null;
		mRunning = false;
	}
	
	/**
	 * Gets the line count.
	 *
	 * @return the line count
	 */
	public int getLineCount() {
		return mLines;
	}
	
	/**
	 * Checks if is running.
	 *
	 * @return true, if is running
	 */
	public boolean isRunning() {
		return mRunning;
	}
	
	/**
	 * On error.
	 *
	 * @param msg the msg
	 * @param e the e
	 */
	public abstract void onError(String msg, Throwable e);
	
	/**
	 * On newline.
	 *
	 * @param line the line
	 */
	public abstract void onNewline(String line);
}